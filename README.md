# Add Wireguard VPN peer helper

### About
This will help you to add new peers (clients) on your Wireguard VPN server.
When used, this script will generate two new files:
 - ```.conf``` file - that contain the client configuration as TEXT and can be imported in Wireguard GUI client application
 - ```.png``` file - it contain (same) client configuration as QRCODE which can be scaned by Wireguard client application

### Usage

```shell
$ sudo ./add_peer.sh <client number> <device name>
```
Where ```client number``` it is generaly an increment from 2 to 254. Based on this number an IP address wil be generated for that specific device. The ```device name``` it is the name of your device. Maybe something like **ws-010** or **boss-latest-iphone**


But before first usage, you need to set the corect values in ```.env``` file.
```shell 
$ cp .env_example .env
$ vim .env
```

### .env file
```bash
# Wireguard interface eg. wg0
INTERFACE=wg0

# Your public IP or domain
SERVER_DOMAIN=wg.example.com

# Listen port. eg: 51820
SERVER_PORT=51820

# At this prefix the <client nuber> will be added. 
# Also this must be on the same subnet as your server
# If your wg0 IP is 172.16.16.1 then prefix must be 172.16.16.
# and this script will add the client number. 
# Eg: if the client number is 2 then IPV4_PREFIX will be 172.16.16.2
IPV4_PREFIX=172.16.16.

# Net mask. If IPV4_MASK is equal to 32 the result will be the IP in CIDR format 172.16.16.2/32
IPV4_MASK=32

# Same as IPv4. If not specified, IPv6 will be ignored in .conf file 
# Use ./ipv6_prefix.sh to generate an IPv6 IP. Is based on https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-debian-11
IPV6_PREFIX=
IPV6_MASK=64

# Can be empty, single or multiple (comma separated) DNS server IPs
DNS_SERVERS="1.1.1.1, 172.16.16.1"

# If is set to 0.0.0.0/0 all client trafic will use this tunnel aka. FULL TUNEL
# If for example AllowedIPs = 172.16.16.0/24 we have a SPLIT TUNEL. Only the local trafic will be routed through VPN.
ALLOWED_IPS="172.16.16.0/24, 10.0.0.0/24"

# Connection keep alive in seconds 
PERSITENT_KEEP_ALIVE=25

# If is set to 'true' (without quote) then file /etc/wireguard/wg0.conf 
# and all files from ./peers folder are stored into vpn-peers.tar
# You can then copy/store this file.
ARCHIVE=true

# If is set to 'true' (without quote) then vpn-peers.tar file is encrpted using GPG into vpn-peers.tar.gpg. 
# You must have gpg installed and configured. 
# You can find a good guide at https://insight.o-o.studio/article/setting-up-gpg.html#strip-secrets. 
# It can be scary at first but not impossible :) 
# After encryption is done vpn-peers.tar file will be deleted. You have encrypted version.
ENCRYPT=true
GPG_RECIPIENT=<your.email@domain.tld>
```

### Credit
This repo is heavily inspired by this gist https://gist.github.com/pR0Ps/eba07c962447139a33e538afbb7b2749

### License
MIT

@2023
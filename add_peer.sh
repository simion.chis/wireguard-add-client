#!/bin/bash

# https://gist.github.com/pR0Ps/eba07c962447139a33e538afbb7b2749


########
# Script
########

set -e

if [ ! -f .env ]; then
    echo -e "\e[31mERROR: .env file does not exists. \e[0m"
    exit 1
fi
source .env

# Modify these per-server
wg_iface="${INTERFACE}"
config_file="/etc/wireguard/$wg_iface.conf"
server_pub_file="/etc/wireguard/publickey"
server_domain="${SERVER_DOMAIN}"
server_port="${SERVER_PORT}"
ipv4_prefix="${IPV4_PREFIX}"
ipv4_mask="${IPV4_MASK}"
ipv6_prefix="${IPV6_PREFIX}"
ipv6_mask="${IPV6_MASK}"
dns_servers=$([ -z "${DNS_SERVERS}" ] && echo "" || echo "DNS = ${DNS_SERVERS}")
keep_alive=$([ -z "${PERSITENT_KEEP_ALIVE}" ] && echo "" || echo "PersistentKeepalive = ${PERSITENT_KEEP_ALIVE}")

# Require root to change wg-related settings
if ! [ "$(id -u)" = "0" ]; then
    echo -e "\e[31mERROR: root is required to configure WireGuard clients\e[0m"
    exit 1
fi

# publickey is required
if [ ! -f "$server_pub_file" ]; then
	echo -e "\e[31mERROR: Maybe the server is not configured yet! We can not find $server_pub_file file.\e[0m"
	exit 1
fi

# Help and basic error checking
if [ $# -ne 2 ] || [ $# -gt 1 -a "$1" == "--help" ]; then
	echo "Usage:"
	echo "$(basename "$0") <client number> <device name>"
	exit 1
fi

# Pull server pubkey from file
server_pub=$(< "$server_pub_file")

# Params
client_number="$1"
name="$2"

# Generate and store keypair
priv=$(wg genkey)
pub=$(echo "$priv" | wg pubkey)

# Create IPv4 addresses based on client ID
client_ipv4="$ipv4_prefix$client_number/$ipv4_mask"
client_ips="$client_ipv4"
# echo -e "Client IPv4 is set to: ${client_ipv4}"

# Create IPv6 addresses based on client ID
if [ ! -z "$ipv6_prefix" ]; then
	client_ipv6="$ipv6_prefix$client_number/$ipv6_mask"
	client_ips="$client_ipv4, $client_ipv6"
	# echo -e "Client IPv6 is set to: ${client_ipv6}"
fi

# Can't add duplicate IPv4s
if grep -q "$client_ipv4" "$config_file"; then
	echo -e "\e[31mERROR: This client number/ip ($client_ipv4) has already been used in the config file.\e[0m"
	exit 1
fi

# Can't add duplicate IPv6s
if [ ! -z "$ipv6_prefix" ] &&  grep -q "$client_ipv6" "$config_file"; then
	echo -e "\e[31mERROR: This client number/ip ($client_ipv6) has already been used in the config file.\e[0m"
	exit 1
fi

# Add peer to config file (blank line is on purpose)
cat >> $config_file <<-EOM

[Peer]
# $name
PublicKey = $pub
AllowedIPs = $client_ips
EOM

# Make client config
client_config=$(cat <<-EOM
[Interface]
PrivateKey = $priv
Address = $client_ips
$dns_servers

[Peer]
PublicKey = $server_pub
AllowedIPs = ${ALLOWED_IPS}
Endpoint = $server_domain:$server_port
$keep_alive
EOM
)

# Output client configuration
echo "########## START CONFIG ##########"
echo "$client_config"
echo "########### END CONFIG ###########"

FILE=peers/${name}.conf
PNG=peers/${name}.png
# Output client configuration to a text file
echo "$client_config" > "$FILE"
if [ -f "$FILE" ]; then
	echo -e "\n\e[32mClient configuration was saved to $FILE.\n\e[0m"
else 
	echo "\nSomething went wrong when saving $FILE."
fi

# Add client to LibreNMS SNMP monitoring
if command -v jq > /dev/null; then
	if [ -f /etc/snmp/wireguard.json ]; then
		cat /etc/snmp/wireguard.json | echo "$(jq '.public_key_to_arbitrary_name.wg0 += {($ARGS.positional[0]):($ARGS.positional[1])}' --args ${pub} ${name})" > /etc/snmp/wireguard.json
	fi
fi

# Output client configuration to a png file (for mobile client)
if command -v qrencode > /dev/null; then
	echo "$client_config" | qrencode -t png -o $PNG
else
	echo "Install 'qrencode' to also generate a QR code of the above config"
fi

if [ "${ARCHIVE}" = true ]; then
	tar -cf vpn-peers.tar peers/ -C / etc/wireguard/${INTERFACE}.conf
	echo -e "\nAll conf files are stored in vpn-peers.tar"
	echo "To extract use: tar -xf vpn-peers.tar"
fi

if command -v gpg > /dev/null; then
 	if [ "${ENCRYPT}" = true ] && [ -f vpn-peers.tar ]; then
		gpg --encrypt --yes  --recipient "${GPG_RECIPIENT}" --output vpn-peers.tar.gpg vpn-peers.tar
		rm -f vpn-peers.tar
		echo "To decrypt use: gpg -o vpn-peers.tar --decrypt vpn-peers.tar.gpg"
	else
		echo -e "\e[31mERROR: You must have ARCHIVE=true in .env file.\e[0m"
	fi
else 
	echo "Install and configure 'gpg'"
fi



# Restart service
echo ""
read -p "Restart 'wg-quick@$wg_iface' ? [y]: " confirm
if [ $confirm == "y" ]; then
	systemctl restart "wg-quick@$wg_iface"
else
	echo "WARNING: 'wg-quick@$wg_iface' will need to be restarted before the new client can connect"
fi

#!/bin/bash

# https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-debian-11

time_stamp=$(date +%s%N)
machine_id=$(cat /var/lib/dbus/machine-id)
hash=$(echo "${time_stamp}${machine_id}" | sha1sum | cut -c 31-41)

first=$(echo $hash | cut -c 1-2)
second=$(echo $hash | cut -c 3-6)
third=$(echo $hash | cut -c 7-11)

# echo $time_stamp
# echo $machine_id
# echo $hash

ipv6_prefix=fd${first}:${second}:${third}::
echo "IPv6 prefix: $ipv6_prefix"
